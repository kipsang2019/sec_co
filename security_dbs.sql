-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 18, 2019 at 04:07 PM
-- Server version: 10.1.39-MariaDB
-- PHP Version: 7.1.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `security_dbs`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `name`, `password`) VALUES
(1, 'Company', 'a24bceeb7a82b39c9cf1483335987020a7631c72');

-- --------------------------------------------------------

--
-- Table structure for table `assignement`
--

CREATE TABLE `assignement` (
  `id` int(11) NOT NULL,
  `client_name` varchar(50) NOT NULL,
  `assign_type` varchar(150) NOT NULL,
  `e_id` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `assignement`
--

INSERT INTO `assignement` (`id`, `client_name`, `assign_type`, `e_id`) VALUES
(2, 'Ujenzi Ltd', 'vehicle checking-night shift', 1),
(3, 'Standard Chattered bank', 'Gate attendant', 2),
(4, 'Ujenzi Ltd', 'Day time shift ', 3),
(5, 'Bidco Ltd', 'Gate keeper', 4),
(6, 'Garden city', 'accountant officer', 5),
(7, 'Garden city', 'Gate keeper', 1);

-- --------------------------------------------------------

--
-- Table structure for table `attendance_records`
--

CREATE TABLE `attendance_records` (
  `id` int(11) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `attendance_status` varchar(100) NOT NULL,
  `dated` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `attendance_records`
--

INSERT INTO `attendance_records` (`id`, `first_name`, `last_name`, `attendance_status`, `dated`) VALUES
(1, 'philiph', 'kirui', 'Present', '2019-07-12'),
(2, 'philiph', 'kirui', 'Present', '2019-07-12'),
(3, 'philiph', 'kirui', 'Present', '2019-07-12'),
(4, 'philiph', 'kirui', 'Present', '2019-07-12'),
(5, 'Berum', 'Tabitha', 'Absent', '2019-07-13'),
(6, 'Berum', 'Tabitha', 'Absent', '2019-07-13'),
(7, 'Berum', 'Tabitha', 'Absent', '2019-07-13'),
(8, 'Berum', 'Tabitha', 'Absent', '2019-07-13'),
(9, 'Berum', 'Tabitha', 'Absent', '2019-07-17'),
(10, 'Berum', 'Tabitha', 'Absent', '2019-07-17'),
(11, 'Berum', 'Tabitha', 'Absent', '2019-07-17'),
(12, 'Berum', 'Tabitha', 'Absent', '2019-07-17'),
(13, 'Berum', 'Tabitha', 'Absent', '2019-07-17'),
(14, 'Frank', 'Chirchir', 'Present', '2019-07-18'),
(15, 'Jentrix', 'chero', 'Present', '2019-07-18'),
(16, 'moses', 'kipsang', 'Absent', '2019-07-18'),
(17, 'philiph', 'kirui', 'Absent', '2019-07-18'),
(18, 'Berum', 'Tabitha', 'Absent', '2019-07-18');

-- --------------------------------------------------------

--
-- Table structure for table `change_task`
--

CREATE TABLE `change_task` (
  `id` int(20) NOT NULL,
  `e_id` int(20) NOT NULL,
  `client_id` int(20) NOT NULL,
  `reason` text NOT NULL,
  `status` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `change_task`
--

INSERT INTO `change_task` (`id`, `e_id`, `client_id`, `reason`, `status`) VALUES
(1, 2, 2, 'The area is in close proximity', ''),
(2, 1, 3, 'I am not in good terms with my bosses', ''),
(3, 4, 4, 'I had no assignment. I was recruited recently', ''),
(4, 5, 5, 'am living not far from Toyoto motors', ''),
(5, 3, 4, 'I dont like this place', '');

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `id` int(11) NOT NULL,
  `client_name` varchar(150) NOT NULL,
  `address` varchar(150) NOT NULL,
  `client_type` varchar(150) NOT NULL,
  `no_of_reqGuards` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`id`, `client_name`, `address`, `client_type`, `no_of_reqGuards`) VALUES
(1, 'CBK', 'Nairobi CBD', 'Bank', 50),
(2, 'Standard chattered bank', 'Kileleshwa', 'bank', 25),
(3, 'Ujenzi Ltd', 'Machakos', 'Construction company', 14),
(4, 'Bidco Ltd', 'Kiambu', 'Manufacturing company', 20),
(5, 'Toyota motors', 'Mombasa road', 'motor vehicle shop', 20),
(6, 'Garden city', 'Homeland', 'mall', 4);

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `employee_id` int(20) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `email` varchar(150) NOT NULL,
  `national_id` int(11) NOT NULL,
  `county` varchar(150) NOT NULL,
  `edu_level` varchar(150) NOT NULL,
  `username` varchar(150) NOT NULL,
  `password` varchar(100) NOT NULL,
  `status` varchar(100) NOT NULL,
  `attendance_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`employee_id`, `first_name`, `last_name`, `email`, `national_id`, `county`, `edu_level`, `username`, `password`, `status`, `attendance_date`) VALUES
(1, 'Frank', 'Chirchir', 'chirchirfrank2018@gmail.com', 34114141, 'Longisa', 'Certificate in criminology', 'frank', '86a8c2da8527a1c6978bdca6d7986fe14ae147fe', 'Approve', '0000-00-00'),
(2, 'Jentrix', 'chero', 'jentychero@gmail.com', 34114155, 'Kakamega', 'Form four', 'chero', '044e643375a824f5e368670296fff7be6014157e', 'Approve', '0000-00-00'),
(3, 'moses', 'kipsang', 'kiptukenya@gmail.com', 34474744, 'Bomet', 'Class 8', 'moses', '76c580f0e3e4c9061b71a0636aee421ebf678b6e', 'Approve', '0000-00-00'),
(4, 'Cate', 'shiko', 'shikocate2019@gmail.com', 34114540, 'Bomet', 'Form four', 'cate', 'f4c9a581f43ba6b6e6e59ca3a2db61dbfa6ee24b', 'Decline', '0000-00-00'),
(5, 'jero', 'winy', 'jero@gmail.com', 123456, 'nakuru', 'degree holder', 'jero', '12567db461c4a9096e16297f880834d5ddf58c27', 'Decline', '0000-00-00'),
(6, 'philiph', 'kirui', 'sang@gmail.com', 2508634, 'Bomet', 'form4', 'philiph', '362d50cf6b90f7b182ec62e024a4c90e67658fd0', 'Approve', '0000-00-00'),
(7, 'Berum', 'Tabitha', 'berum7tab@gmail.com', 33238462, 'nakuru', 'degree holder', 'berum', '6975a3f5d4b8a1039f21c8350072cdc90ad6878b', 'Approve', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `job_applicants`
--

CREATE TABLE `job_applicants` (
  `job_id` int(20) NOT NULL,
  `job_title` varchar(150) NOT NULL,
  `phone_number` int(17) NOT NULL,
  `age` int(4) NOT NULL,
  `e_id` int(20) NOT NULL,
  `status` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `job_applicants`
--

INSERT INTO `job_applicants` (`job_id`, `job_title`, `phone_number`, `age`, `e_id`, `status`) VALUES
(1, 'security guard', 711414114, 23, 2, ''),
(2, 'Security guard', 706114111, 22, 1, ''),
(3, 'Security guard', 744146652, 21, 3, ''),
(4, 'Security guard', 724114165, 23, 4, ''),
(5, 'Accountant officer', 2147483647, 26, 5, ''),
(6, 'security gaurd', 700405936, 28, 6, ''),
(7, 'ICT OFFICER', 796439063, 23, 7, '');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` int(20) NOT NULL,
  `message` text NOT NULL,
  `e_id` int(20) NOT NULL,
  `sent_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `m_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `message`, `e_id`, `sent_time`, `m_id`) VALUES
(1, 'hello. I request my shift to be changed please. I want to work during day time', 2, '0000-00-00 00:00:00', 0),
(2, 'I propose for salary increment', 1, '0000-00-00 00:00:00', 0),
(3, 'I am grateful for your considerations', 1, '0000-00-00 00:00:00', 0),
(4, 'I am grateful. You accepted my request of change in assignment', 2, '0000-00-00 00:00:00', 0),
(5, 'thanks for your reply', 1, '0000-00-00 00:00:00', 0),
(6, 'hi there', 1, '2019-07-01 14:56:14', 0),
(7, ' my supervisor is claiming i change my shift from night to day shift', 5, '2019-07-02 18:30:09', 0),
(8, 'hi', 6, '2019-07-06 10:34:41', 0),
(9, 'hi', 4, '2019-07-12 17:50:07', 0),
(10, 'hey', 1, '2019-07-13 13:23:31', 0),
(11, 'hi admin', 3, '2019-07-18 14:00:36', 0);

-- --------------------------------------------------------

--
-- Table structure for table `on_leave`
--

CREATE TABLE `on_leave` (
  `id` int(20) NOT NULL,
  `leave_reason` text NOT NULL,
  `start_date` varchar(20) NOT NULL,
  `end_date` varchar(20) NOT NULL,
  `e_id` int(20) NOT NULL,
  `status` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `on_leave`
--

INSERT INTO `on_leave` (`id`, `leave_reason`, `start_date`, `end_date`, `e_id`, `status`) VALUES
(1, 'I lost a close relative', '2019-07-02T12:00', '2019-07-11T18:00', 3, 'declined'),
(2, 'I got sick', '2019-07-03', '2019-07-08', 1, 'Approved'),
(3, 'am not feeling well', '2019-07-01', '2019-07-17', 4, 'Approved'),
(4, 'i need meternity', '2019-07-31', '2019-08-31', 5, ''),
(5, 'compasionate ieave', '2019-07-10', '2019-07-25', 6, '');

-- --------------------------------------------------------

--
-- Table structure for table `replies`
--

CREATE TABLE `replies` (
  `id` int(20) NOT NULL,
  `reply_text` text NOT NULL,
  `e_id` int(20) NOT NULL,
  `admin_id` int(10) NOT NULL,
  `reply_date` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `replies`
--

INSERT INTO `replies` (`id`, `reply_text`, `e_id`, `admin_id`, `reply_date`) VALUES
(1, 'Your shift will be changed soon. We value our guards', 2, 1, '2019-07-01 08:55:24.680561'),
(2, 'the company is still under negotiation with its partners on salary increment ', 1, 1, '2019-07-01 08:55:24.680561'),
(3, 'Welcome frank', 1, 1, '2019-07-01 08:55:24.680561'),
(4, 'Welcome jentrix', 2, 1, '0000-00-00 00:00:00.000000'),
(5, 'hey how is work', 1, 1, '0000-00-00 00:00:00.000000'),
(6, 'the mater will be sorted out  before the end of today ', 5, 1, '0000-00-00 00:00:00.000000'),
(7, 'erfgh', 5, 1, '0000-00-00 00:00:00.000000'),
(8, 'yes how r u\r\n', 4, 1, '0000-00-00 00:00:00.000000'),
(9, 'hey\r\nwhat is up?\r\n', 1, 1, '0000-00-00 00:00:00.000000'),
(10, 'hi sang', 3, 1, '0000-00-00 00:00:00.000000');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `assignement`
--
ALTER TABLE `assignement`
  ADD PRIMARY KEY (`id`),
  ADD KEY `e_id` (`e_id`);

--
-- Indexes for table `attendance_records`
--
ALTER TABLE `attendance_records`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `change_task`
--
ALTER TABLE `change_task`
  ADD PRIMARY KEY (`id`),
  ADD KEY `client_id` (`client_id`),
  ADD KEY `e_id` (`e_id`);

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`employee_id`);

--
-- Indexes for table `job_applicants`
--
ALTER TABLE `job_applicants`
  ADD PRIMARY KEY (`job_id`),
  ADD KEY `e_id` (`e_id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `e_id` (`e_id`),
  ADD KEY `m_id` (`m_id`);

--
-- Indexes for table `on_leave`
--
ALTER TABLE `on_leave`
  ADD PRIMARY KEY (`id`),
  ADD KEY `e_id` (`e_id`);

--
-- Indexes for table `replies`
--
ALTER TABLE `replies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `e_id` (`e_id`),
  ADD KEY `admin_id` (`admin_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `assignement`
--
ALTER TABLE `assignement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `attendance_records`
--
ALTER TABLE `attendance_records`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `change_task`
--
ALTER TABLE `change_task`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `employee_id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `job_applicants`
--
ALTER TABLE `job_applicants`
  MODIFY `job_id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `on_leave`
--
ALTER TABLE `on_leave`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `replies`
--
ALTER TABLE `replies`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `assignement`
--
ALTER TABLE `assignement`
  ADD CONSTRAINT `assignement_ibfk_1` FOREIGN KEY (`e_id`) REFERENCES `employees` (`employee_id`);

--
-- Constraints for table `change_task`
--
ALTER TABLE `change_task`
  ADD CONSTRAINT `change_task_ibfk_1` FOREIGN KEY (`client_id`) REFERENCES `clients` (`id`),
  ADD CONSTRAINT `change_task_ibfk_2` FOREIGN KEY (`e_id`) REFERENCES `employees` (`employee_id`);

--
-- Constraints for table `job_applicants`
--
ALTER TABLE `job_applicants`
  ADD CONSTRAINT `job_applicants_ibfk_1` FOREIGN KEY (`e_id`) REFERENCES `employees` (`employee_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `messages`
--
ALTER TABLE `messages`
  ADD CONSTRAINT `messages_ibfk_1` FOREIGN KEY (`e_id`) REFERENCES `employees` (`employee_id`);

--
-- Constraints for table `on_leave`
--
ALTER TABLE `on_leave`
  ADD CONSTRAINT `on_leave_ibfk_1` FOREIGN KEY (`e_id`) REFERENCES `employees` (`employee_id`);

--
-- Constraints for table `replies`
--
ALTER TABLE `replies`
  ADD CONSTRAINT `replies_ibfk_1` FOREIGN KEY (`e_id`) REFERENCES `employees` (`employee_id`),
  ADD CONSTRAINT `replies_ibfk_2` FOREIGN KEY (`admin_id`) REFERENCES `admin` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
