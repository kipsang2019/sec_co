<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="../css/bootstrap/css/bootstrap.min.css">
</head>
<body  style="background: #a7a742;">
	<div class="row">
		<div class="col-sm-4"></div>
		<div class="col-sm-4">

			<h1 style="color:#afec0c; padding-top: 50px;">Supervisor login</h1>
			<form action="supervisor_login.php" method="POST">
				<input class="form-control" type="text" name="username" placeholder="Username"><br><br>
				<input class="form-control" type="password" name="pwd" placeholder="Password"><br><br>
				<button class="btn btn-success" name="login_btn">Login</button>
			</form>

		</div>
		
	</div>
</body>
</html>