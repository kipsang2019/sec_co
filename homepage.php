<?php 
	include 'header.php';

 ?>
 	<div class="cont_pp">
 		<form action="guard_reports.php" method="POST" accept-charset="utf-8">
 			<input type="hidden" name="e_id" value="<?php echo $_SESSION['e_id'] ?>">
 			<input type="hidden" name="f_name" value="<?php echo $_SESSION['f_name'] ?>">
 			<input type="hidden" name="l_name" value="<?php echo $_SESSION['l_name'] ?>">
 			<button class="btn btn-info" name="report">Your reports</button>
 		</form>
	
	<?php if (isset($_SESSION['message'])): ?>
		<?php 
			echo $_SESSION['message'];
			unset($_SESSION['message']);
		 ?>
	<?php endif ?><br><br>
	<h2>Profile</h2>
	<?php if (isset($_SESSION['f_name'])): ?>
		Name:
		<?php 
			echo $_SESSION['f_name']." ".$_SESSION['l_name'];
		 ?>
	<?php endif ?><br><br>
	
	<?php if (isset($_SESSION['email'])): ?>
		Email:
		<?php 
			echo $_SESSION['email'];
		 ?>
	<?php endif ?><br><br>
	<?php if (isset($_SESSION['n_id'])): ?>
		National Id:
		<?php 
			echo $_SESSION['n_id'];
		 ?>
	<?php endif ?><br><br>
	<?php if (isset($_SESSION['county'])): ?>
		From:
		<?php 
			echo $_SESSION['county']." county";
		 ?>
	<?php endif ?><br><br>
	<form action="dbs/main_conn.php" method="POST" accept-charset="utf-8">
		<button style="background: #e63224;" name="logout">Logout</button>
	</form>
 	</div>
	


 <?php 
	include 'footer.php';

 ?>