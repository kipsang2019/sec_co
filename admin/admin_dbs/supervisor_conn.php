<?php 
	
	include 'main_conn.php';

	if (isset($_POST['submit'])) {
		
		$first_name = mysqli_real_escape_string($db, $_POST['first_name']);
		$last_name = mysqli_real_escape_string($db, $_POST['last_name']);
		$email = mysqli_real_escape_string($db, $_POST['email']);
		$username = mysqli_real_escape_string($db, $_POST['username']);
		$pass = mysqli_real_escape_string($db, $_POST['pass']);

		if (empty($first_name) || empty($last_name) || empty($email) || empty($username ) || empty($pass)) {
			header("Location: ../supervisor.php?some fields=empty!!");
			exit();
		}else{
			//check if email is taken
			$mail = "SELECT email FROM supervisors WHERE email='$email'";
			$result = mysqli_query($db, $mail);
			$checkResult = mysqli_num_rows($result);
			if ($checkResult > 0) {
				header("Location: ../supervisor.php?email=taken!!");
				exit();
			}else{
				//check if username is taken
				$user = "SELECT username FROM supervisors WHERE username='$username'";
				$results = mysqli_query($db, $user);
				$confirmResult = mysqli_num_rows($results);
				if ($confirmResult > 0) {
					header("Location: ../supervisor.php?username=taken!!");
					exit();
				}else{

					$pwd = sha1($pass);

					//insert to table
					$sql = "INSERT INTO supervisors(first_name,last_name,email,username,pass)
					VALUES('$first_name','$last_name','$email','$username','$pwd')";
					mysqli_query($db, $sql);
					header("Location: ../supervisor.php?Insert=success!!");
					exit();
				}
			}
		}
	}


 ?>