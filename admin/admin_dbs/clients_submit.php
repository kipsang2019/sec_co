<?php 
	
	include 'main_conn.php';

	if (isset($_POST['add'])) {
		
		$client_name = mysqli_real_escape_string($db, $_POST['client_name']);
		$location = mysqli_real_escape_string($db, $_POST['location']);
		$firm_type = mysqli_real_escape_string($db, $_POST['firm_type']);
		$guards_requested = mysqli_real_escape_string($db, $_POST['guards_requested']);

		if (empty($client_name) || empty($location) || empty($firm_type) || empty($guards_requested)) {
			header("Location: ../clients.php?Some fields are empty");
			exit();
		}else{
			//check if client exists
			$sql = "SELECT client_name FROM clients WHERE client_name='$client_name'";
			$result = mysqli_query($db, $sql);
			$checkResult = mysqli_num_rows($result);
			if ($checkResult > 0) {

				echo "<script>alert('Client already exist')</script>";
				echo "<script>window.open('../clients.php', '_self')</script>";				
				exit();
			}else{
				$insert = "INSERT INTO clients(client_name,address,client_type,no_of_reqGuards) VALUES('$client_name','$location','$firm_type','$guards_requested')";
				mysqli_query($db, $insert);
				
				echo "<script>alert('new client inserted successfully')</script>";
				echo "<script>window.open('../clients.php', '_self')</script>";
				exit();
			}
		}
	}

 ?>