<?php 
	include 'header.php';

 ?>
<div class="left" >
<h2>Post Jobs</h2>
	
	<form action="admin_dbs/upload_jobs.php" method="POST" enctype="multipart/form-data">
		<input type="file" name="file"><br><br>
		<button class="btn btn-success" name="post_btn">Post</button>
	</form>

	<div class="col-sm-6">
		<p>uploaded vacancies</p>
		<?php 

			$files = scandir("../uploads/jobApplications");
			for ($i=2; $i < count($files); $i++) { 
				echo '<a href="../uploads/jobApplications/'.$files[$i].'">'.$files[$i].'</a><br><br>';
			}
		 ?>
		 
	</div>
</div>
  <div class="row">
 	<div class="right">
 		
<table>
	<h2>Job  Applicants</h2>
	<thead>
		<tr>
			<th>First name</th>
			<th>Last name</th>
			<th>Email</th>
			<th>County</th>
			<th>Approvals</th>
			<th>Save</th>
		</tr>
	</thead>
	<tbody>

		
<?php 
	
	//$db = mysqli_connect('localhost', 'root', '', 'security_dbs');

	$sql = "SELECT employee_id,first_name,last_name,email,county,e_id,job_applicants.status 
	FROM employees JOIN job_applicants ON employee_id=e_id";
	$result = mysqli_query($db, $sql);
	while ($row = mysqli_fetch_assoc($result)) {
		echo '
			 		<tr>
			 			<td>'.$row['first_name'].'</td>
			 			<td>'.$row['last_name'].'</td>
			 			<td>'.$row['email'].'</td>
			 			<td>'.$row['county'].'</td>
			 			<form action="admin_dbs/approval.php" method="POST">  
			 			<td>
			 				<select name="approval">

			 					<option>'.$row['status'].'</option>
								<option>Approve</option>
								<option>Decline</option>
							</select>
			 			</td>
			 			<input type="hidden" name="e_id" value="'.$row['e_id'].'">
			 			<td><button name="save_btn">Save</button></td>
			 			</form>
			 		</tr>
			 	';
	}

 ?>

 </tbody>
</table>

 	</div>
 	