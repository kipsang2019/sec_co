  <?php
//include("db.php");
include "header.php";

require '..\fpdf181/fpdf.php';
if (isset($_POST['print'])) {
	
	//include '../dbs/db_conn.php';
	ob_start();

	$sql = "SELECT * FROM attendance_records WHERE dated='$_POST[date]'";
	$result = mysqli_query($db, $sql);

	$date= "SELECT dated FROM attendance_records WHERE dated = '$_POST[date]'";
	$results = mysqli_query($db, $date);
	$r = mysqli_fetch_assoc($results);

	$pdf = new FPDF('P','mm','A4');
	$pdf -> AddPage();
	$pdf -> SetFont('Times','B',12);
	$pdf -> Image('../images/logo.jpeg',80,20,0,40);
	$pdf -> SetTextColor(204,0,0);
	$pdf -> Cell(180,10,'ANTI~CRIME COMPANY LTD',0,1,'C');
	$pdf -> SetTextColor(0,0,0);
	$pdf -> Ln(40);
	$pdf -> Cell(160,20,'ATTENTANCE RECORD',1,1,'C');
	$pdf -> Cell(50,10,'Date',1,0,'C');
	$pdf -> Cell(110,10,$r['dated'],1,1);
	$pdf ->SetFillColor(148, 184, 184);
	$pdf -> Cell(50,10,'First name',1,0,0,'B');
	$pdf -> Cell(50,10,'Last name',1,0,0,'B');
	$pdf -> Cell(60,10,'Attendance status',1,1,0,'B');
	$pdf -> SetFont('Times','',12);

	while ($row = mysqli_fetch_assoc($result)) {
		$pdf -> Cell(50,5,$row['first_name'],1,0);
		$pdf -> Cell(50,5,$row['last_name'],1,0);
		$pdf -> Cell(60,5,$row['attendance_status'],1,1);
	}

	/*summary
	present*/

	$sql1 = "SELECT * FROM attendance_records WHERE attendance_status='Present'
	AND dated='$_POST[date]'";
	$result1 = mysqli_query($db, $sql1);
	$pdf -> Ln(10);
	$pdf -> SetFont('Times','B',12);
	$pdf -> Cell(160,10,'Attendance Summary',0,1,'C');
	
	$pdf -> Cell(160,10,'Present guards record',1,1,'C');
	$pdf ->SetFillColor(148, 184, 184);
	$pdf -> Cell(50,10,'First name',1,0,0,'B');
	$pdf -> Cell(50,10,'Last name',1,0,0,'B');
	$pdf -> Cell(60,10,'Attendance status',1,1,0,'B');
	$pdf -> SetFont('Times','',12);

	while ($row1 = mysqli_fetch_assoc($result1)) {
		$pdf -> Cell(50,5,$row1['first_name'],1,0);
		$pdf -> Cell(50,5,$row1['last_name'],1,0);
		$pdf -> Cell(60,5,$row1['attendance_status'],1,1);

	}

	/*Absent guards*/
	$sql1 = "SELECT * FROM attendance_records WHERE attendance_status='Absent'
	AND dated='$_POST[date]'";
	$result1 = mysqli_query($db, $sql1);
	$pdf -> Ln(10);
	$pdf -> SetFont('Times','B',12);
	
	$pdf -> Cell(160,10,'Absent guards record',1,1,'C');
	$pdf ->SetFillColor(148, 184, 184);
	$pdf -> Cell(50,10,'First name',1,0,0,'B');
	$pdf -> Cell(50,10,'Last name',1,0,0,'B');
	$pdf -> Cell(60,10,'Attendance status',1,1,0,'B');
	$pdf -> SetFont('Times','',12);

	while ($row1 = mysqli_fetch_assoc($result1)) {
		$pdf -> Cell(50,5,$row1['first_name'],1,0);
		$pdf -> Cell(50,5,$row1['last_name'],1,0);
		$pdf -> Cell(60,5,$row1['attendance_status'],1,1);

	}


	$pdf -> output();
	ob_end_flush();
}
   

?>

<div class="panel panel-default">

	<div class="panel panel-heading">
	
	

	  

	<div class="panel panel-body">

		<form action="demo2.php" method="post">

			<table class="table table-striped">
				<tr>
				<th>#Serial Number</th> <th>First Name</th> <th>Last Name</th> <th>Attendance status</th>
				</tr>

				<?php

				$result=mysqli_query($db, "select * from attendance_records where dated='$_POST[date]'");
				$Serialnumber=0;
				$counter=0;

				while ($row=mysqli_fetch_array($result))

				 {
					$Serialnumber++;
				
				?>

				<tr>
				  <td> <?php echo $Serialnumber; ?></td>
				  <td> <?php echo $row['first_name']; ?>
				  </td>
				  <td> <?php echo $row['last_name']; ?>
				  </td>

				  <td>
				  	
					<?php 
						echo $row['attendance_status'];
					 ?>
				  </td>
				</tr>

				<?php
				$counter++;
				 }
				?>
			</table>

			
		</form>
	</div>

	</div>

</div>