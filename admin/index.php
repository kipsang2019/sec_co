<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="../css/bootstrap/css/bootstrap.min.css">
</head>
<body style="background: #a7a742;">

	<div class="row">
		<div class="col-sm-4"></div>
		<div class="col-sm-4">

			<h1 style="color:#edff00; padding-top: 35px;">HR Admin login</h1>
			<form action="admin_dbs/index_submit.php" method="POST">
				<input class="form-control" type="text" name="username" placeholder="Username"><br><br>
				<input class="form-control" type="password" name="pwd" placeholder="Password"><br><br>
				<button class="btn btn-success" name="login_btn">Login</button><br>
				For supervisor click:  <a href="../supervisor/login.php" style="color: 39eaf0;">Supervisor login</a>
			</form>

		</div>
		
	</div>
</body>
</html>