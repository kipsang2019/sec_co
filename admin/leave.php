<?php 
	include 'header.php';

 ?>

 <h2>Leave</h2>

<table>
	<thead>
		<tr>
			<td>First name</td>
			<td>Last name</td>
			<td>Leave reason</td>
			<td>From</td>
			<td>To</td>
			<td>Status</td>
			<td>Save</td>
		</tr>
	</thead>
	<tbody>
		<?php 

			$sql = "SELECT first_name,last_name,leave_reason,start_date,end_date,e_id,on_leave.status 
			FROM employees JOIN on_leave 
			ON employee_id=e_id";

			$result = mysqli_query($db, $sql);
			while ($row = mysqli_fetch_assoc($result)) {
				echo '<tr>
						<td>'.$row['first_name'].'</td>
						<td>'.$row['last_name'].'</td>
						<td>'.$row['leave_reason'].'</td>
						<td>'.$row['start_date'].'</td>
						<td>'.$row['end_date'].'</td>
						<form action="admin_dbs/update_leave.php" method="POST">
							<td><input type="text" name="status" value="'.$row['status'].'"></td>
							<input type="hidden" name="e_id" value="'.$row['e_id'].'">
							<td><button class="btn btn-success" name="save_update">Save</button></td>
						</form>
					</tr>';
			}
		 ?>
		
	</tbody>
</table>

