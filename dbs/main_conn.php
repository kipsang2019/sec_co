<?php 
	
	include 'db_conn.php';

	if (isset($_POST['submit'])) {
		
		$first_name = mysqli_real_escape_string($db, $_POST['first_name']);
		$last_name = mysqli_real_escape_string($db, $_POST['last_name']);
		$email = mysqli_real_escape_string($db, $_POST['email']);
		$national_id = mysqli_real_escape_string($db, $_POST['national_id']);
		$county = mysqli_real_escape_string($db, $_POST['county']);
		$dob = mysqli_real_escape_string($db, $_POST['dob']);
		$username = mysqli_real_escape_string($db, $_POST['username']);
		$pass1 = mysqli_real_escape_string($db, $_POST['pass1']);
		$pass2 = mysqli_real_escape_string($db, $_POST['pass2']);

		if (empty($first_name) || empty($last_name) || empty($email) || empty($national_id) || empty($county) || empty($dob) || empty($username) ||  empty($pass1) || empty($pass2)) {

			echo "<script>alert('Some fields are empty')</script>";
			echo "<script>window.open('../signup.php', '_self')</script>";
			
			exit();
		}else{
			//check if national id is taken
			$sql = "SELECT national_id FROM employees WHERE national_id='$national_id'";
			$result = mysqli_query($db, $sql);
			$checkResult = mysqli_num_rows($result);
			if ($checkResult > 0) {

				echo "<script>alert('ID is already taken')</script>";
   				echo "<script>window.open('../signup.php?', '_self')</script>";				
				exit();
			}else{

				//validate email
				if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
 			 	echo "<script>alert('Email format is incorrect')</script>";
				echo "<script>window.open('../signup.php', '_self')</script>"; 
 			 exit();
            }else{
				//check if pass match
				if ($pass2 !== $pass1) {
					header("Location: ../signup.php?Passwords dont match!!");
					exit();
				}else{
					//check if username exists
					$u_name = "SELECT username FROM employees WHERE username='$username'";
					$record = mysqli_query($db, $u_name);
					if (mysqli_num_rows($record) > 0) {
						header("Location: ../signup.php?Username has been taken!!");
						exit();
					}else{
						//hash the password
						$pwd = sha1($pass1);

						//insert details to dbs
						$query = "INSERT INTO employees(first_name,last_name,email,national_id,county,dob,username,password) VALUES('$first_name','$last_name','$email','$national_id','$county','$dob','$username','$pwd')";
						mysqli_query($db,$query);

						echo "<script>alert('signup is success')</script>";
   						echo "<script>window.open('../login.php?', '_self')</script>";				
						exit();
						
					}
					
				}
			}

			}
		}
	}

	if (isset($_POST['login'])) {
		$username = mysqli_real_escape_string($db, $_POST['username']);
		$pwd = mysqli_real_escape_string($db, $_POST['pwd']);

		if (empty($username) || empty($pwd)) {
			echo "<script>alert('some fields are empty')</script>";
   			echo "<script>window.open('../login.php?', '_self')</script>";				
			exit();
			
		}else{
			$pass = sha1($pwd);
			$sql = "SELECT * FROM employees WHERE username='$username' AND password='$pass'";
			$results = mysqli_query($db, $sql);
			$row = mysqli_fetch_assoc($results);
			if (mysqli_num_rows($results) == 1) {
				$_SESSION['e_id'] = $row['employee_id'];
				$_SESSION['f_name'] = $row['first_name'];
				$_SESSION['l_name'] = $row['last_name'];
				$_SESSION['email'] = $row['email'];
				$_SESSION['n_id'] = $row['national_id'];
				$_SESSION['county'] = $row['county'];
				$_SESSION['message'] = 'You are now logged in!!';

				echo "<script>alert('You are logged in successfully')</script>";
   				echo "<script>window.open('../homepage.php?', '_self')</script>";				
				exit();
				
			}else{

				echo "<script>alert('Incorrect username or password')</script>";
   				echo "<script>window.open('../login.php?', '_self')</script>";				
				exit();
			}
		}
	}

	if (isset($_POST['logout'])) {
		session_start();
		session_unset();
		session_destroy();
		header("Location: ../login.php");
		exit();
	}

 ?>