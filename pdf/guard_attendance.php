<?php 
	
	include '../dbs/db_conn.php';
	require '..\fpdf181/fpdf.php';

	$first_name = mysqli_real_escape_string($db, $_POST['f_name']);
 	$last_name = mysqli_real_escape_string($db, $_POST['l_name']);

	$sql = "SELECT * FROM attendance_records
 	WHERE first_name='$first_name' AND last_name='$last_name'";
 	$result = mysqli_query($db, $sql);

	$pdf = new FPDF('P','mm','A5');
	$pdf -> AddPage();
	$pdf -> SetFont('Times','B',12);
	$pdf -> Image('../images/logo.jpeg',55,20,0,40);
	$pdf -> SetTextColor(204,0,0);
	$pdf -> Cell(120,10,'Lavington Security  Ltd',0,1,'C');
	$pdf -> SetTextColor(0,0,0);
	$pdf -> Ln(40);
	$pdf -> SetFillColor(220, 220, 220);
	$pdf -> Cell(20,10,'Name:',0,0,0,'B');
	$pdf -> Cell(20,10,$first_name,0,0,0,'B');
	$pdf -> Cell(20,10,$last_name,0,1,0,'B');
	$pdf -> Ln(10);
	$pdf -> Cell(100,10,'Your attendance records',1,1,'C');
	$pdf -> SetFillColor(148, 184, 184);
	$pdf -> Cell(50,10,'Date',1,0,0,'B');
	$pdf -> Cell(50,10,'Attendance status',1,1,0,'B');
	$pdf -> SetFont('Times','',12);
	$pdf ->SetFillColor(180,180,180);
	while ($row = mysqli_fetch_assoc($result)) {
		$pdf -> Cell(50,5,$row['dated'],1,0);
		$pdf -> Cell(50,5,$row['attendance_status'],1,1);

	}

	$pdf -> output();

 ?>