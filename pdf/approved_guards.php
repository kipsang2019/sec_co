<?php 
	
	include '../dbs/db_conn.php';
	require '..\fpdf181/fpdf.php';

	$sql = "SELECT * FROM attendance_records";
	$result = mysqli_query($db, $sql);

	$pdf = new FPDF('P','mm','A4');
	$pdf -> AddPage();
	$pdf -> SetFont('Times','B',12);
	$pdf -> Image('../images/logo.jpeg',80,20,0,40);
	$pdf -> SetTextColor(204,0,0);
	$pdf -> Cell(180,10,'ANTI~CRIME COMPANY LTD',0,1,'C');
	$pdf -> SetTextColor(0,0,0);
	$pdf -> Ln(40);
	$pdf -> Cell(160,20,'CONSOLIDATED ATTENTANCE RECORD',1,1,'C');
	$pdf ->SetFillColor(148, 184, 184);
	$pdf -> Cell(40,10,'First name',1,0,0,'B');
	$pdf -> Cell(40,10,'Last name',1,0,0,'B');
	$pdf -> Cell(40,10,'Date',1,0,0,'B');
	$pdf -> Cell(40,10,'Attendance status',1,1,0,'B');
	$pdf -> SetFont('Times','',12);
	$pdf ->SetFillColor(180,180,180);
	while ($row = mysqli_fetch_assoc($result)) {
		$pdf -> Cell(40,5,$row['first_name'],1,0);
		$pdf -> Cell(40,5,$row['last_name'],1,0);
		$pdf -> Cell(40,5,$row['dated'],1,0);
		$pdf -> Cell(40,5,$row['attendance_status'],1,1);

	}

	//summary
	//Present
	$sql1 = "SELECT * FROM attendance_records WHERE attendance_status='Present'";
	$result1 = mysqli_query($db, $sql1);
	$pdf -> Ln(10);
	$pdf -> SetFont('Times','B',12);
	$pdf -> Cell(160,10,'Attendance Summary',0,1,'C');
	
	$pdf -> Cell(160,10,'Present guards record',1,1,'C');
	$pdf ->SetFillColor(148, 184, 184);
	$pdf -> Cell(40,10,'First name',1,0,0,'B');
	$pdf -> Cell(40,10,'Last name',1,0,0,'B');
	$pdf -> Cell(40,10,'Date',1,0,0,'B');
	$pdf -> Cell(40,10,'Attendance status',1,1,0,'B');
	$pdf -> SetFont('Times','',12);

	while ($row1 = mysqli_fetch_assoc($result1)) {
		$pdf -> Cell(40,5,$row1['first_name'],1,0);
		$pdf -> Cell(40,5,$row1['last_name'],1,0);
		$pdf -> Cell(40,5,$row1['dated'],1,0);
		$pdf -> Cell(40,5,$row1['attendance_status'],1,1);

	}

	//Absent
	$sql1 = "SELECT * FROM attendance_records WHERE attendance_status='Absent'";
	$result1 = mysqli_query($db, $sql1);
	$pdf -> Ln(10);
	$pdf -> SetFont('Times','B',12);
	
	$pdf -> Cell(160,10,'Absent guards record',1,1,'C');
	$pdf ->SetFillColor(148, 184, 184);
	$pdf -> Cell(40,10,'First name',1,0,0,'B');
	$pdf -> Cell(40,10,'Last name',1,0,0,'B');
	$pdf -> Cell(40,10,'Date',1,0,0,'B');
	$pdf -> Cell(40,10,'Attendance status',1,1,0,'B');
	$pdf -> SetFont('Times','',12);
	while ($row1 = mysqli_fetch_assoc($result1)) {
		$pdf -> Cell(40,5,$row1['first_name'],1,0);
		$pdf -> Cell(40,5,$row1['last_name'],1,0);
		$pdf -> Cell(40,5,$row1['dated'],1,0);
		$pdf -> Cell(40,5,$row1['attendance_status'],1,1);

	}

	$pdf -> output();

 ?>