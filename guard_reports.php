<?php 
	include 'header.php';

 ?>

 <h2>Guard's reports</h2>
 <h4>Attendance records</h4>
 <form action="pdf/guard_attendance.php" method="POST" accept-charset="utf-8">
 	<input type="hidden" name="e_id" value="<?php echo $_SESSION['e_id'] ?>">
	<input type="hidden" name="f_name" value="<?php echo $_SESSION['f_name'] ?>">
	<input type="hidden" name="l_name" value="<?php echo $_SESSION['l_name'] ?>">
	<button class="btn btn-info" name="report_btn">Print</button>
 </form>
 <table style="width: 40%;">
 	<thead>
 		<tr>
 			<th>Date</th>
 			<th>Attendance status</th>
 		</tr>
 	</thead>
 	<tbody>
 	
 <?php 
 	$first_name = mysqli_real_escape_string($db, $_POST['f_name']);
 	$last_name = mysqli_real_escape_string($db, $_POST['l_name']);

 	$sql = "SELECT * FROM attendance_records
 	WHERE first_name='$first_name' AND last_name='$last_name'";
 	$result = mysqli_query($db, $sql);
 	while ($row = mysqli_fetch_assoc($result)) {
 		
 		echo '<tr>
	 			<td>'.$row['dated'].'</td>
	 			<td>'.$row['attendance_status'].'</td>
	 		</tr>';
 	}

  ?>
  </tbody>
 </table>
<br>

 <?php 
	include 'footer.php';

 ?>
